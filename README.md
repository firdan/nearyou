# nearYou app
showing a recommendation places based on user location and current weather.

## Requirements

* python 2.7
* bower
* pip
* virtualenv (optional but recommended)

## Installation
For development:

1. run `pip install -r requirements/base.txt`
2. from `nearyou/static/` run `bower install` and `compass compile`
3. run `python nearyou/manage.py runserver`

For server, as it depend on server configuration and the server os, please refer to django documentation regarding deployment.

## App Structure

```
/ -|- nearyou
      |- ads # ads backend model
      |- api # backend api
      |- media # for uploaded user picture
      |- nearyou # django web settings
      |- places # models for places model
      |- static # static files
      |- templates # django templates for frontend
      |- weather # weathernews API wrapper
   |- requirements
   |- simple_frontend # trial for frontend
   |- README.md  # this readme
```

## Credits
Bimasakti team:

* Firdan
* Dea
* Cakra
