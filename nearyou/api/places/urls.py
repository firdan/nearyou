from django.conf.urls import patterns, include, url
from .views import PlaceList, PlaceDetail

urlpatterns = [
    url(r'^$', PlaceList.as_view()),
    url(r'^(?P<pk>\d+)/$', PlaceDetail.as_view()),
]
