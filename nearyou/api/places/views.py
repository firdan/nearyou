from django.shortcuts import render
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import Distance
from rest_framework import generics

from weather.models import WeatherNewsWrapper
from places.models import Place
from .serializers import PlaceSerializer

DEFAULT_RADIUS = 10


class PlaceList(generics.ListAPIView):
    """
    List Place query, using GET parameter

    lat -- lattitude
    lon -- longitude
    """
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer
    paginate_by = 50

    def get_queryset(self):
        lat = self.request.GET.get('lat')
        lon = self.request.GET.get('lon')
        if lat and lon:
            lat = float(lat)
            lon = float(lon)
            ref_location = Point(lon, lat)
            weather_code = WeatherNewsWrapper().get_weather(ref_location)
            places = Place.objects.filter(point__distance_lte=(ref_location, D(km=DEFAULT_RADIUS)))
            if weather_code:
                places = places.filter(weather=weather_code)
            return places.annotate(distance=Distance('point', ref_location)).order_by('distance')
        return generics.ListAPIView.get_queryset(self)


class PlaceDetail(generics.RetrieveAPIView):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer
