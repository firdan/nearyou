from rest_framework import serializers
from places.models import Place


class PlaceSerializer(serializers.ModelSerializer):
    lat = serializers.FloatField(source='get_lat')
    lon = serializers.FloatField(source='get_lon')

    class Meta:
        model = Place
        exclude = ('created', 'point')
