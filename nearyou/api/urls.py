from django.conf.urls import url, include

urlpatterns = [
    url(r'places/', include('api.places.urls')),
    url(r'ads/', include('api.ads.urls'))
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
