from rest_framework import serializers
from ads.models import Ads


class AdsSerializer(serializers.ModelSerializer):
    lat = serializers.FloatField(source='get_lat')
    lon = serializers.FloatField(source='get_lon')

    class Meta:
        model = Ads
        exclude = ('id', 'created', 'point')
