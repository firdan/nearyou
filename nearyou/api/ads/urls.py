from django.conf.urls import patterns, include, url
from .views import AdsList, AdsDetail

urlpatterns = [
    url(r'^$', AdsList.as_view()),
    url(r'^(?P<pk>\d+)/$', AdsDetail.as_view()),
]
