from django.shortcuts import render
from rest_framework import viewsets
from ads.models import Ads
from .serializers import AdsSerializer
from django.shortcuts import render
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.contrib.gis.db.models.functions import Distance
from rest_framework import generics

from weather.models import WeatherNewsWrapper
from ads.models import Ads
from .serializers import AdsSerializer

DEFAULT_RADIUS = 1


class AdsList(generics.ListAPIView):
    """
    List Ads query, using GET parameter

    lat -- lattitude
    lon -- longitude
    """
    queryset = Ads.objects.all()
    serializer_class = AdsSerializer
    paginate_by = 50

    def get_queryset(self):
        lat = self.request.GET.get('lat')
        lon = self.request.GET.get('lon')
        if lat and lon:
            lat = float(lat)
            lon = float(lon)
            ref_location = Point(lon, lat)
            weather_code = WeatherNewsWrapper().get_weather(ref_location)
            result = Ads.objects.filter(point__distance_lte=(ref_location, D(km=DEFAULT_RADIUS)), weather=weather_code).annotate(distance=Distance('point', ref_location)).order_by('distance')

            if len(result) > 0:
                return result
            return Ads.objects.filter(weather=weather_code)
        return generics.ListAPIView.get_queryset(self)


class AdsDetail(generics.RetrieveAPIView):
    queryset = Ads.objects.all()
    serializer_class = AdsSerializer
