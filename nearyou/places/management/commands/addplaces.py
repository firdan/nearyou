import csv

from django.core.management.base import BaseCommand, CommandError
from django.contrib.gis.geos import Point
from ...models import Place



class Command(BaseCommand):
    args = '<filename>'
    help = 'insert csv file to Place model'

    def handle(self, *args, **options):
        # get the file
        weather = (
            ("any",'a'),
            ("sunny", 's'),
            ("heavy rain", 'hr'),
            ("rain", 'r'),
            ("cloudy", 'c'),
            ("heavy snow", 'hs'),
            ("hot",'h'),
            ("thunder storm",'ts')
        )
        filename = args[0]
        with open(filename, 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                try:
                    ref_location = Point(float(row['lon']), float(row['lat']))
                    weather_code = dict(weather)[row['weather']]
                    Place.objects.get_or_create(
                        name=row['name'],
                        description=row['description'],
                        category=row['category'],
                        weather=weather_code,
                        point=ref_location)
                    self.stdout.write("processing %s ..."%(row['name']))
                except ValueError:
                    pass
