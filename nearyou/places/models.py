from __future__ import unicode_literals

from django.contrib.gis.db import models
from django.contrib.gis.db.models.manager import GeoManager

from weather.models import WEATHER


class Place(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1000)
    category = models.CharField(max_length=30)
    weather = models.CharField(max_length=2, choices=WEATHER)
    image = models.ImageField(upload_to='places/', null=True)
    point = models.PointField()

    created = models.DateTimeField(auto_now_add=True)
    objects = GeoManager()

    def get_lat(self):
        return self.point.coords[1] if self.point else None

    def get_lon(self):
        return self.point.coords[0] if self.point else None

    def __unicode__(self):
        return self.name
