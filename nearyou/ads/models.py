from __future__ import unicode_literals

from django.contrib.gis.db import models
from weather.models import WEATHER


class Ads(models.Model):
    name = models.CharField(max_length=50)
    link = models.URLField()
    weather = models.CharField(max_length=2, choices=WEATHER)
    point = models.PointField(null=True)
    image = models.ImageField(upload_to='ads/')

    created = models.DateTimeField(auto_now_add=True)

    def get_lat(self):
        return self.point.coords[1] if self.point else None

    def get_lon(self):
        return self.point.coords[0] if self.point else None

    class Meta:
        verbose_name_plural = "ads"

    def __unicode__(self):
        return self.name
