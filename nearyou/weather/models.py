from __future__ import unicode_literals

import requests
from requests.exceptions import Timeout
from django.contrib.gis.db import models

ANY = 'a'
SUNNY = 's'
RAIN = 'r'
HEAVY_RAIN = 'hr'
CLOUDY = 'c'
SNOW = 'sw'
HEAVY_SNOW = 'hs'
HOT = 'h'

WEATHER = (
    (ANY, "any"),
    (SUNNY, "sunny"),
    (RAIN, "rain"),
    (HEAVY_RAIN, "heavy rain"),
    (CLOUDY, "cloudy"),
    (SNOW, "snow"),
    (HEAVY_SNOW, "heavy snow"),
    (HOT, "hot"),
)


class WeatherNewsWrapper(object):
    """ wrapper for weathernews API """
    url = "http://weathernews.jp/v/SOIASIA/weather.cgi"

    def translate(self, weather_id):
        #TODO: need to improve with other weather
        if weather_id >= 100 and weather_id < 200:
            return SUNNY
        if weather_id >= 200 and weather_id < 300:
            return CLOUDY
        if weather_id >= 300 and weather_id < 325:
            return RAIN
        if weather_id >= 500 and weather_id < 600:
            return HOT
        if weather_id >= 800 and weather_id <= 865:
            return HEAVY_RAIN
        return ANY

    def get_weather(self, point):
        lon = point[0]
        lat = point[1]

        # fetch the url
        try:
            r = requests.get("http://weathernews.jp/v/SOIASIA/weather.cgi?latlon=%s/%s" % (lat, lon))
            result = r.json()
            weather_id = result[0]['observation']['WX']
            return self.translate(weather_id)
        except Timeout:
            return None
