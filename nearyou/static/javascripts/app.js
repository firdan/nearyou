(function($){
  'use strict';
  var userLocation={lat:0,lon:0};
  var tempId='';
  var iconTranslate = {
    's': "wi-day-sunny",
    'r': "wi-rain",
    'hr': "wi-thunderstorm",
    'c': "wi-cloudy",
    'sw': "wi-snow",
    'hs': "wi-snow-wind",
    'h': "wi-hot"
  }
  var defaultLocation = {coords:{latitude:-6.890278,longitude:107.61}}

  $("#content").on('click','.show-map',function(e){
    e.preventDefault();
    $("#loading").show()
    var id = $(this).data('id');
    var dest={lat:$(this).data('lat'),lon:$(this).data('lon')}
    $("#content").html('<iframe width="100%" height="450" frameborder="0" style="overflow:hidden; width:100%; border: none" src="https://www.google.com/maps/embed/v1/directions?origin='+userLocation.lat+","+userLocation.lon+'&destination='+dest.lat+","+dest.lon+'&key=AIzaSyBaJbs1llO_Gfq7k-yKGoKkFFmJL0UMGas" allowfullscreen></iframe>');

    // goback button, this is code Hack! not good idea for production!
    // anyway, need to fix this
    tempId = "#place-"+id;
    $('#content').append('<a class="btn btn-default" onclick="window.location.reload(true);window.location.hash=\''+tempId+'\';" href="#place-'+id+'">go back</a>')
    $("#loading").hide()
  });

  $("#about").on('click',function(e){
    e.preventDefault();
    var aboutTmpl = Hogan.compile($('#about-tmpl').html());
    var rendered = aboutTmpl.render();
    $("#content").html(rendered);
  });

  function getLocation() {
      // clear the content
      $("#content").html('');
      $('#loading').show();
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(fetchData);
      } else {
          alert("Geolocation is not supported by this browser.");
      }
  }

  function fetchData(position) {
    var pos = position;
    userLocation.lat=pos.coords.latitude;
    userLocation.lon=pos.coords.longitude;

    $.get('/api/ads/?lat='+pos.coords.latitude+'&lon='+pos.coords.longitude,
      function(data){
        // get the places;
        var adsTmpl = Hogan.compile($('#ads-tmpl').html());
        for (var i=0; i< data.length; i++){
          var place = data[i];
          var context = {
            image_link:place.image,
            link:place.link,
          }
          var rendered = adsTmpl.render(context);
          $("#content").prepend(rendered);
        }
      })

    $.get('/api/places/?lat='+pos.coords.latitude+'&lon='+pos.coords.longitude,
      function(data){
        // get the places;
        var placeTmpl = Hogan.compile($('#place-tmpl').html());
        if (data.length == 0){
          $("#content").append("<p>sorry, cannot find best locations from your current position</p>");
        } else {
          for (var i=0; i< data.length; i++){
            var place = data[i];

            // use dummy image if image is empty
            var image = "http://lorempixel.com/800/600/city";
            if (place.image){
              image = place.image;
            }

            var context = {
              id:place.id,
              image_link:image,
              title:place.name,
              description:place.description,
              category:place.category,
              weather_icon:iconTranslate[place.weather],
              lat:place.lat,
              lon:place.lon
            }
            var rendered = placeTmpl.render(context);
            $("#content").append(rendered);
            $('#loading').hide();
          }
        }
      })
  }

  $("#get-location").on('click',function(e){
    getLocation();
  });

  fetchData(defaultLocation);

})(jQuery);
